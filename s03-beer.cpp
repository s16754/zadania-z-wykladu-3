#include <string>
#include <iostream>
#include <vector>
int main(int argc, char** argv)
{
	int i = (atoi(argv[1]));
	int first_digit = i;
    while(i!=0){
		std::cout<< i << "bottles of beer on the wall, "<< i <<" bottles of beer. \n";
		std::cout<<"Take one down, pass it around, "<<i-1<<" bottles of beer on the wall... \n";
		i--;
	}
	std::cout<<"No more bottles of beer on the wall, no more bottles of beer.\n";
	std::cout<<"Go to the store and buy some more, "<<first_digit<<" bottles of beer on the wall...";
	
    return 0;
}
