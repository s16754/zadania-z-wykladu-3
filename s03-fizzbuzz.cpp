#include <string>
#include <iostream>
auto wyliczanie(int const limit) -> void
{
	for (auto i = 1; i <= limit; ++i) {
		if (i%3==0 && i%5==0)
			std::cout<<i<<" FizzBuzz \n";
		else if(i%5==0)
			std::cout<<i<<" Buzz \n";
		else if(i%3==0)
			std::cout<<i<<" Fizz \n";
		else
			std::cout<<i<<" \n";
	}
}

auto main() -> int
{
	std::cout<<"Podaj liczbe: ";
	int podana_liczba;
	std::cin>>podana_liczba;
    
	wyliczanie(podana_liczba);
	return 0;
}
