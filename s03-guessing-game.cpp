#include <string>
#include <iostream>
#include <random>
auto main() -> int
{
    int liczba = rand() % 100 + 1;
    int zgadywanka;
    int condition = 0; 
    std::cout<<"Zgadnij losowa liczbe: \n";
    std::cout<<liczba;
    do{
        std::cin>>zgadywanka;
        if (liczba > zgadywanka)
            std::cout<<"Too small! \n";
        else if (liczba < zgadywanka)
            std::cout<<"Too big! \n";
        else if (liczba == zgadywanka){
            std::cout<<"BINGO!";
            condition = 1;
        }
    }while (condition==0);
    
	return 0;
}
