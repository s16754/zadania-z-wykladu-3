#include <string>
#include <iostream>
auto odliczanie(int const limit) -> void
{
	for (auto i= limit; i>=0; i--) {
		std::cout<<i<< "... \n";
	}
}

auto main(int argc) -> int
{
	odliczanie(argc);
	return 0;
}
